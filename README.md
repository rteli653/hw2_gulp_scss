# hw2_gulp_scss

My homework to consolidate the theoretical knowledge in practice.


#### The following technologies are used:
- layout analysis - Figma;
- HTML layout - Webstorm;
- connected Google fonts libraries;
- project assembly: Node.JS with Gulp manager
- Used Gulp packages:
• gulp-sass;
• browser sync;
• gulp-js-minify;
• gulp-uglyfy;
• gulp-clean-css;
• gulp-concat;
• gulp imagemin;
• gulp-autoprefixer;

####Developer:
- Roman Teliha
